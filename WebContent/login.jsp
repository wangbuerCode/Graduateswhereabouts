<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!--          引入bootStrap文件里的css和js      将jQuery.js加入到/bootstrap/js中-->
<link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/bootstrap/js/jQuery.js"></script>
<script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.js"></script>
<style type="text/css">
	body{
			background-image: url("images/background.jpg");
			text-align:center;
		}
	.box{ 
			width:370px; 
			margin:25px auto 80px auto;
			background:#FFFFFF; 
			border-radius:6px; 
			border-bottom:3px solid #C5C5C5; 
			border-top:3px solid #C5C5C5; 
			padding:30px 40px;
		}
	.loginForm{ 
		float:center; 
		display:inline; 
		position:relative; 
		}
	i{
	  text-align: center;
	  margin-top: -4px;
	  margin-right: 5px;
	}
	.line01{ 
		height:1px; 
		line-height:1px; 
		font-size:1px; 
		background:#C5C5C5; 
		clear:both; 
		overflow:hidden;
	}
	.footer{ text-align:center; padding:5px 0; color:#666666; border-top:1px solid #FFFFFF;}
	.font01{}
	.font12{ font-size:12px; line-height:22px;}
	.color999 a,.color999 a:link{color:#999999;}
	.color999 a:hover{color:#333333;}
	font{
		font-family: FangSong;
		font-weight: bold;
	}
	
</style>
<script type="text/javascript">
function checkform(){
	var userName=document.getElementById("userName").value;
	var password=document.getElementById("password").value;
	var userTypeId=document.getElementById("userTypeId").value;
	if(userName==null||userName==""){
		document.getElementById("error").innerHTML="用户名不能为空！";
		return false;
	}
	if(password==null||password==""){
		document.getElementById("error").innerHTML="密码不能为空！";
		return false;
	}
	if(userTypeId=="-1"){
		document.getElementById("error").innerHTML="请选择用户类别！";
		return false;
	} 
	return true;
}
function Change(){
		if(document.getElementById("userTypeId").value=="1"){
			$("#gsdj").hide();
		}else{
			$("#gsdj").show();
		}
		return true;
	}
	function resetInfo(){
		document.getElementById("userName").value="";
		document.getElementById("password").value="";
		document.getElementById("userTypeId").value="-1";
		document.getElementById("error").value="";
	}
</script> 
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>登录</title>
</head>
<body> 
	<div class="row-fluid" id="contents" style="margin-top: 20px;margin-bottom: 100px">
<%--	         <div class="span12"> <img src="images/bg1.jpg" style=" border: 5px solid #6495ED; padding: 5px; background: #fff;"></div>--%>
	</div>
<div class="box clearfix">
<div class="loginForm">
	<div>
		<font size="5px">毕业生就业管理系统</font>
	</div>
	<form action="${pageContext.request.contextPath}/user?way=login" method="post" onsubmit="return checkform() ">
			<!-- <input type="hidden" id="action" name="action" value="login"> -->
		<p>
			<i class="icon-user"></i><input type="text" id="userName" name="userName" placeholder="请输入用户名"  style="margin-top: 9px"></input>
		</p>
		<p>
			<i class="icon-lock "></i><input type="password" id="password" name="password" placeholder="请输入密码" style="margin-top: 9px"></input>
		</p>
		<i class="icon-check"></i><select id="userTypeId" name="userTypeId" onclick="Change()" style="width: 220px;margin-top: 9px">
          					<option value="-1"  selected="selected" >请选择登陆身份</option>
          			       	<!-- 	默认没有选中 -->
							<option value="1">管理员</option>
						    <option value="2">学生</option>
							<option value="3">公司</option>
			</select>
			</br>
				<!-- 	点击公司之后显示登录，注册，和返回  -->
					<!-- 注册 -->
					<input id="gsdj"  type="button"  class="btn  btn-info" value="注册" onclick="javascript:window.location='${pageContext.request.contextPath}/registe.jsp'"/>
					<!-- 登录 -->
					<input type="submit" class="btn  btn-primary" value="登录" />
					<!--   返回 -->
					<input type="button" class="btn  btn-danger" value="重置" onclick="resetInfo()"/>
		</form>
		&nbsp;&nbsp;<font id="error" color="red">${error}</font>
		<div class="line01">&nbsp;</div>
		<div class="footer">
		</div>
	 </div>
</div>
</body>
</html>