<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>注册</title>
<link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/bootstrap/js/jQuery.js"></script>
<script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.js"></script>
<style type="text/css">
	label{
		text-align: center;
	}
</style>
</head>
<script type="text/javascript">
function Change(){
	if(document.getElementById("userTypeId").value=="2"){
		$(".xszc").show();
	}else{
		$(".xszc").hide();
	}
	if(document.getElementById("userTypeId").value=="3"){
		$("#gszc").show();
	}else{
		$("#gszc").hide();
	}
}
function checkform(){
			/* 学生和公司共有的部分 */
	var userName=document.getElementById("userName").value;
	var password=document.getElementById("password").value;
			/* 学生独有的部分 */
	var stuAge=document.getElementById("stuAge").value;
	var stuTel=document.getElementById("stuTel").value;
	var stuGrade=document.getElementById("stuGrade").value;
	var stuMajor=document.getElementById("stuMajor").value;
			/* 公司独有的部分 */
	var companyContent=document.getElementById("companyContent").value;
	        /*  获取用户Id */
	var userTypeId=document.getElementById("userTypeId").value;
	var reg=/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,18}$/;
	var phone=/^[1][3,4,5,7,8][0-9]{9}$/;
	if(userName==null||userName==""){
		document.getElementById("error").innerHTML="用户名不能为空！";
		return false;
	}
	if(password==null||password==""||!reg.test(password)){
		document.getElementById("error").innerHTML="密码不能为空！";
		document.getElementById("error").innerHTML="密码必须为6-18位数字加字母！";
		return false;
	}                
	if(userTypeId=="-1"){
		document.getElementById("error").innerHTML="请选择用户类别！";
		return false;
	}else if(userTypeId=="2"){
		if(stuAge==null||stuAge==""){
			document.getElementById("error").innerHTML="年龄不能为空！";
			return false;
		}
		if(stuTel==null||stuTel==""||!phone.test(stuTel)){
			document.getElementById("error").innerHTML="电话不能为空！";
			document.getElementById("error").innerHTML="请输入正确的电话号码！";
			return false;
		}
		if(stuGrade==null||stuGrade==""){
			document.getElementById("error").innerHTML="年级不能为空！";
			return false;
		}
		if(stuMajor==null||stuMajor==""){
			document.getElementById("error").innerHTML="专业不能为空！";
			return false;
		}
	}else{
		if(companyContent==null||companyContent==""){
			document.getElementById("error").innerHTML="公司描述不能为空！";
			return false;
		}
	}
	return true;
}

</script>

<body>
	<div class="container">
		<div class="row-fluid">
		    <div class="span4"></div>
		 		<div class="span4" style="margin-top: 100px">
			 		<form action="${pageContext.request.contextPath}/user?way=registe" method="post" onsubmit="return checkform()">
				 		<table class="table table-bordered" >
				 			<label style="text-align: center;font-size:30px;color: purple;font-weight:bold;">用户注册</label>
				 			<tr>
				 				<td><label>用户名</label></td>
				 				<td><input type="text" id="userName" name="userName"></input></td>
				 			</tr>
				 			<tr>
				 				<td><label>密码</label></td>
				 				<td><input type="password" id="password" name="password"></input></td>
				 			</tr>
				 			<tr>
				 				<td><label>用户类别</label></td>
				 				<td>
					 				<select id="userTypeId" name="userTypeId" style="width: 220px;margin-top: 9px" onclick="Change()">
			          					<option value="-1"  selected="selected" >请选择注册身份</option>
			          			       	<!-- 	默认没有选中 -->
									    <option value="2">学生</option>
										<option value="3">公司</option>
									</select>
				 				</td>
				 			</tr>
				 				<tr id="gszc" style="display: none">
				 					<td><label>公司描述</label></td>
					 				<td><textarea  rows="4" cols="3" id="companyContent" name="companyContent" ${company.companyContent }></textarea></td>
				 				</tr>
					 			<tr class="xszc" style="display: none">
					 				<td><label>性别</label></td>
					 				<td>
					 					&nbsp;&nbsp;<input type="radio"  id="stuSex" name="stuSex" checked value="男"/>男
     									&nbsp;&nbsp;<input type="radio"  id="stuSex" name="stuSex" value="女" />女
     								</td>
					 			</tr>
						 		<tr  class="xszc" style="display: none">
					 				<td><label>年龄</label></td>
					 				<td><input type="text" id="stuAge" name="stuAge" value="${student.stuAge }"></input></td>
					 			</tr>
					 			<tr  class="xszc" style="display: none">
					 				<td><label>电话</label></td>
					 				<td><input type="text" id="stuTel" name="stuTel" value="${student.stuTel }"></input></td>
					 			</tr>
					 			<tr  class="xszc" style="display: none">
					 				<td><label>年级</label></td>
					 				<td><input type="text" id="stuGrade" name="stuGrade" value="${student.stuGrade }"></input></td>
					 			</tr>
					 			<tr  class="xszc" style="display: none">
					 				<td><label>专业</label></td>
					 				<td><input type="text" id="stuMajor" name="stuMajor" value="${student.stuMajor }"></input></td>
					 			</tr>
				 				<tr>
				 					<td>
				 						<label>操作</label>
				 					</td> 
				 					<td>
				 						<input id="zc" type="submit" class="btn btn-primary" value="注册"/>&nbsp;&nbsp;
										<input type="button" class="btn btn-primary" value="返回" onclick="javascript:history.back()"/>
										&nbsp;&nbsp;<font id="error" color="red">${error }</font>
				 					</td>
								</tr>
						</table>
				 	</form>
			 </div>
		  <div class="span4"></div>
		</div>
	</div>
</body>
</html>